# Pick Language v2 hook for PyroCMS

Extended version of the core PyroCMS pick_language.php hook

- version - 1.0.0
- Authors - Lukas Angerer - lukas.angerer@getunik.com
- Website - [getunik AG](http://www.getunik.com)

## What is it?

The pick language v2 hook pretty much does the same thing as the core language hook; it is invoked in the *pre_controller* stage of the request and determines the language that will be used throughout the remainder of the request, for example in the *CURRENT_LANGUAGE* constant.

The advantages of this language picker over the built-in PyroCMS language picker is that it supports language selection by domain name and path prefix and it is configurable. In particular, the priorities of the different ways to select a request's language can be configured to fit the needs of the website. It is also somewhat better structured as the default language picker and because of that much easier to extend with additional rules to select the language.

## Installation

1. Put the `pick_language_v2.php` file in your `PROJECTROOT/system/cms/hooks` directory.
2. Replace the default language hook
```
$hook['pre_controller'][] = array(
	'function' => 'pick_language',
	'filename' => 'pick_language.php',
	'filepath' => 'hooks'
);
```
with the equivalent v2 version
```
$hook['pre_controller'][] = array(
	'function' => 'pick_language',
	'filename' => 'pick_language_v2.php',
	'filepath' => 'hooks',
	'params' => array(
		'querystring' => NULL,
		'session' => NULL,
		'cookie' => NULL,
		'browser' => NULL
	)
);
```

At this point, the language selection should work just like it did before. To make some use of the new features provided by this new language picker, check out the sections on configuration.

## Configuration

The configuration comes in the form of the `params` array in the hook config. It is an ordered map from language picker rules to rule configuration arrays:

```
'rule_name' => array(...)
or
'rule_name' => NULL
```

Not all rules have a specific configuration, but the order in which they appear in the array determines their priorities. The language picker will go through all the rules in order until one responds with a valid language code. The rest of the rules are skipped, so the first rule that gets a result wins.

If none of the rules responds with a valid language, the configured `default_language` is used and a valid language is any language present in your `supported_languages` configuration. It is recommended that you **remove** all languages that your website does not support from the `supported_languages` configuration.

### Language by query string (`querystring`)
Picks the language from the **`lang`** query string variable. If it is present, the language selection is successful, otherwise the picker will continue with the next rule.

### Language by session (`session`)
Picks the language from the **`lang_code`** session variable. Whatever the language resulting from the language picker is, it is always stored in the `lang_code` session variable before the language picker returns.

### Language by cookie (`cookie`)
Picks the language from the **`lang_code`** cookie. Depending on your PyroCMS setup, this cookie might get set automatically.

### Language by browser accept language (`browser`)
Goes through the request's **HTTP_ACCEPT_LANGUAGE** http header (set by the browser) to find the first matching valid language.

### Language by path prefix (`pathprefix`)
Picks the language from the first segment of the request URL. e.g. a request for '/en/test' would pick 'en' as the language (provided that it is a valid language in the current PyroCMS installation).

### Language by domain (`domain`)
Picks the language from a set of defined domain name - language mappings. This rule covers the common case where different language versions of a website should be accessed through specific language domains.

#### Settings
The settings for the domain rule is a map from regular expressions to language identifiers. If the `$_SERVER['SERVER_NAME']` variable matches one of the provided regular expressions, the corresponding language is selected.
```
'domain' => array(
	'germandomain\.de' => 'de',
	'englishdomain\.net' => 'en',
	'frenchdomain\.com' => 'fr',
),
```

## Change Log

### v-1.0.0 - initial release
* all the basic equivalence functionality with original language picker
* `pathprefix` rule
* `domain` rule
